# Machine Learning Course 2017 #

This repository contains files for the 2017 Machine Learning Course 

### What you will find ###

* Jupyter notebooks containing skeleton code for tutorials
* Jupyter notebooks containing possible solutions to problems
